package org.bitbucket.maksim_lebedev.db_util_wrapper;

import org.apache.commons.dbcp2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * For tests {@link ConnectionFactory}
 * Created by lebedev on 05.08.16.
 */
public class TestConnectionFactory {

    private static final Logger LOG = LoggerFactory.getLogger(TestConnectionFactory.class);

    private static String DB_URL;
    private static String DB_USER;
    private static String DB_PSW;

    private static class DbcpDataSource {

        private static final BasicDataSource dataSource;

//        private static final PoolingDataSource poolingDataSource;

        static {
            dataSource = new BasicDataSource();
            dataSource.setUrl(DB_URL);
            dataSource.setUsername(DB_USER);
            dataSource.setPassword(DB_PSW);
        }

//        static {
//            DriverManagerConnectionFactory connectionFactory = new DriverManagerConnectionFactory(DB_URL, DB_USER, DB_PSW);
//            PoolableConnectionFactory factory = null;
//            try {
//                factory = new PoolableConnectionFactory(connectionFactory, ObjectName.getInstance("org.moneta", "connectionPool", "TestPool"));
//            } catch (MalformedObjectNameException e) {
//                e.printStackTrace();
//            }
//            assert factory != null;
//            GenericObjectPool<PoolableConnection> pool = new GenericObjectPool<>(factory);
//            poolingDataSource = new PoolingDataSource<>(pool);
//        }
    }

    public static void initDb(String dbUrl, String dbUser, String dbPsw) {

        DB_URL = dbUrl;
        DB_USER = dbUser;
        DB_PSW = dbPsw;

        ConnectionFactory.dataSourceFactory = () -> DbcpDataSource.dataSource;
    }

    public static void initProfileDb() {
        Properties prop = new Properties();
        try {
            prop.load(TestConnectionFactory.class.getResourceAsStream("db.properties"));
            initDb(prop.getProperty("db.url"), prop.getProperty("db.user"), prop.getProperty("db.password"));
        } catch (IOException e) {
            LOG.error("Properties file \"{}\" is not found", "db.properties");
        }
    }
}
