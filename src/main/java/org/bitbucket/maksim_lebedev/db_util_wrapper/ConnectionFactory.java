package org.bitbucket.maksim_lebedev.db_util_wrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by lebedev on 05.08.16.
 */
public class ConnectionFactory {

    private static final Logger LOG = LoggerFactory.getLogger(ConnectionFactory.class);

    private static final ThreadLocal<Connection> TRANSACT_CONN = new ThreadLocal<>();

    private static class JndiDataSource {

        private static final DataSource dataSource;

        static {
            try {
                InitialContext ctx = new InitialContext();
                dataSource = (DataSource) ctx.lookup("java:/comp/env/jdbc/db_name");
            } catch (Exception ex) {
                LOG.error("DataSource initialization failed", ex);
                throw new IllegalStateException("DataSource initialization failed", ex);
            }
        }
    }

    interface DataSourceFactory {
        DataSource getDataSource();
    }

    static DataSourceFactory dataSourceFactory = () -> JndiDataSource.dataSource;

    public static DataSource getDataSource() {
        return dataSourceFactory.getDataSource();
    }

    public static Connection getConnection(boolean isReadOnly) throws SQLException {
        Connection conn = getDataSource().getConnection();
        if (isReadOnly) {
            conn.setReadOnly(true);
        }
        return conn;
    }

    public static Connection getTxConnection() throws SQLException {
        Connection conn = TRANSACT_CONN.get();
        if (conn != null) {
            return conn;
        }
        conn = getDataSource().getConnection();
        conn.setAutoCommit(false);
        TRANSACT_CONN.set(conn);
        return conn;
    }

    public static void closeTx(Connection conn) {
        close(conn);
        TRANSACT_CONN.set(null);
    }

    public static void close(Connection conn) {
        if (conn != null) {
            try {
                if (conn.isReadOnly()) {
                    conn.setReadOnly(false);  // restore NOT readOnly before return to pool
                }
            } catch (SQLException e) {
                LOG.warn("Close connection error", e);
            } finally {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.warn("Close connection error", e);
                }
            }
        }
    }
}
