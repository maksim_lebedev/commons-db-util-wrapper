package org.bitbucket.maksim_lebedev.db_util_wrapper;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by lebedev on 05.08.16.
 */
public interface SqlExecutor<T> {

    T run(Connection conn) throws SQLException;
}
