package org.bitbucket.maksim_lebedev.db_util_wrapper;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by lebedev on 05.08.16.
 */
public class Sql {

    private static final Logger LOG = LoggerFactory.getLogger(ConnectionFactory.class);

    private static final QueryRunner QUERY_RUNNER = new QueryRunner();
    private static final ResultSetHandler<Long> KEY_HANDLER = new ScalarHandler<>();

    public static <T> T execute(boolean isReadOnly, SqlExecutor<T> executor) {
        try (Connection conn = ConnectionFactory.getConnection(isReadOnly)) {
            return executor.run(conn);
        } catch (SQLException e) {
            LOG.error("DATA_BASE error", e);
            throw new IllegalStateException(e);
        }
    }

    public static void execute(boolean isReadOnly, SqlExecutorVoid executor) {
        execute(isReadOnly, getWrapperExecutor(executor));
    }

    public static void executeInTx(SqlExecutorVoid executor) {
        executeInTx(getWrapperExecutor(executor));
    }

    public static <T> T executeInTx(SqlExecutor<T> executor) {
        Connection conn = null;
        try {
            conn = ConnectionFactory.getTxConnection();
            T res = executor.run(conn);
            conn.commit();
            return res;
        } catch (Throwable e) {
            throw rollback(conn, e);
        } finally {
            ConnectionFactory.closeTx(conn);
        }
    }

    private static IllegalStateException rollback(Connection conn, Throwable e) {
        try {
            if (conn != null) {
                conn.rollback();
            }
            LOG.warn("Rollback transaction", e);
            return new IllegalStateException(e);
        } catch (SQLException se) {
            se.setNextException(new SQLException(e));
            LOG.error("Unable to rollback transaction", se);
            return new IllegalStateException("Unable to rollback transaction", se);
        }
    }

    private static SqlExecutor<Void> getWrapperExecutor(final SqlExecutorVoid voidExecutor) {
        return conn -> {
            voidExecutor.run(conn);
            return null;
        };
    }

    public static long insert(Connection conn, final String insertSql, final Object... params) throws SQLException {
        return QUERY_RUNNER.insert(conn, insertSql, KEY_HANDLER, params);
    }

    public static int update(Connection conn, final String updateSql, final Object... params) throws SQLException {
        return QUERY_RUNNER.update(conn, updateSql, params);
    }

    public static <T> T query(Connection conn, final String sql, final ResultSetHandler<T> rsh, final Object... params) throws SQLException {
        return QUERY_RUNNER.query(conn, sql, rsh, params);
    }

    public static <T> T queryForValue(Connection conn, final String sql, final Object... params) throws SQLException {
        return query(conn, sql, new ScalarHandler<T>(), params);
    }

    public static <T> T queryForValue(final String sql, final Object... params) throws SQLException {
        return query(sql, new ScalarHandler<T>(), params);
    }

    public static int update(final String updateSql, final Object... params) {
        return execute(false, (SqlExecutor<Integer>) conn -> update(conn, updateSql, params));
    }

    public static <T> T query(final String sql, final ResultSetHandler<T> rsh, final Object... params) {
        return execute(true, (SqlExecutor<T>) conn -> query(conn, sql, rsh, params));
    }

    public static long insert(final String insertSql, final Object... params) {
        return execute(false, (SqlExecutor<Long>) conn -> insert(conn, insertSql, params));
    }
}
